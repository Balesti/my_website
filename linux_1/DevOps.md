## Part 1. Установка ОС

- Прикрепляю скриншот с выводом команды cat /etc/issue
  
![вывод команды cat /etc/issue](/images/Part1.png)

## Part 2. Создание пользователя

- Прикрепляю скриншот с вызовом команды создания пользователя

![Создание пользователя](/images/Part2_2.png)

- Прикрепляю скриншот с выводом команды cat /etc/passwd
  
![вывод команды cat /etc/passwd](/images/Part2.png)

## Part 3. Настройка сети ОС

- Задал название машины вида user-1: sudo hostnamectl set-hostname user-1
- Установил временную зону: sudo timedatectl set-timezone Europe/Moscow
- Вывел названия сетевых интерфейсов: ifconfig. Lo - виртуальный интерфейс по умолчанию для отладки сетевых программ и запуска серверных приложений на локальной машине.
- Узнал ip адресс устройства: ip address. То, что после inet-.
- Внешний ip address - curl ifconfig.co. Внутренний - nslookup localhost. DHCP - Dynamic Host Configuration Protocol.
- Выставил всю конфигурацию. cd /etc/netplan. sudo vim 00-installer-config.yaml. sudo netplan apply.

![00-installer-config.yaml](/images/Part3.png) 

## Part 4. Обновление ОС

- Скриншот с выводом команды sudo apt update
  
![вывод команды sudo apt update](/images/Part4.png)

## Part 5. Использование команды sudo

- Команда sudo позволяет выполнить команду от имени другого пользователя. Как правило от пользователя root для повышения прав доступа для обыного пользователя.

- Скриншот с измененным hostname от имени пользователя kentbaha2(Из Part2)
  
![измененный hostname](/images/Part5.png)

## Part 6. Установка и настройка службы времени

- Скриншот с выводом команды timedatectl show
  
![вывод timedatectl show](/images/Part6.png)

## Part 7.Установка и использование текстовых редакторов

- Vim. Для выхода из редактора вышел из режима insert ctrl + [ и ввел команду :wq
  
![Vim выход с сохр.](/images/Part7_1.png)

- Nano. Для выхода из редактора нажал ctr + X и Y.
  
![nano выход с сохр.](/images/Part7_2.png)

- Joe. Для выхода из редактора нажал ctrl + K  и y.
  
![joe выход с сохр.](/images/Part7_3.png)

- Vim. Для выхода без сохранений вышел из режима insert ctrl + [ и ввел команду :q!
  
![Vim выход без сохр.](/images/Part7_4.png)

- Nano. Для выхода без сохранения из редактора нажал ctrl + X и N.
  
![nano выход без сохр.](/images/Part7_5.png)

- Joe. Для выхода из редактора нажал ctrl + K  и n.
  
![joe выход без сохр.](/images/Part7_6.png)

- Vim. Для поиска слова ввел команду /.
  
![Vim поиск](/images/Part7_7.png)

- Vim. Для замены слова ввел команду s/word/newWord
  
![Vim поиск и замена](/images/Part7_8.png)

- Nano. Для поиска слова ввел команду ctrl + W.
  
![nano поиск](/images/Part7_9.png)

- Nano. Для замены слова ввел команду ctrl + \\.
  
![nano поиск и замена](/images/Part7_10.png)

- Joe. Для поиска слова ввел команду ctrl + KF.
  
![joe поиск](/images/Part7_11.png)

- Joe. Для замены слова ввел команду ctrl + KF и выбор пункта замены.
  
![joe поиск и замена](/images/Part7_12.png)

## Part 8 Установка и базовая настройка сервиса SSHD

- sudo apt install openssh-server
- sudo systemctl enable ssh
- sudo vim /etc/ssh/sshd_config
- ps -e | grep sshd. -e - все процессы. grep - поиск слова.

- Вывод команды netstat -tan
  
![вывод netstat](/images/Part8.png)

- Флаг -a - перечисление всех портов. Флаг -t - перечисление всех TCP портов. Флаг -n - поиск без резолва IP/имен.

- Proto - имя протокола. Recv-Q - получено кол-во байт. Send-Q - отправленно кол-во байт.Local Address - локальный адрес участвующий в соединении. Foreigh Address - внешний адрес, участвующий в создании соединения. State - состояние соединения.

- 0.0.0.0 означает, что в соединии может использоваться любые IP-адреса.

## Part 9. Установка и использование утилит top, htop

- uptime - 34 min
- количество авторизованных пользователей - 1
- общая загрузка системы - 0.01
- общее количество процессов - 30
- загрузку cpu - 0.7%
- загрузку памяти - 255M
- pid процесса занимающего больше всего памяти - 791
- pid процесса, занимающего больше всего процессорного времени - 791
- Скрин с выводом htop, отсортированному по PID, PERCENT_CPU, PERCENT_MEM, TIME

![Отфильтрованный htop](/images/Part9_1.png)

- Скрин htop, отфильтрованный для процесса sshd

![Отфильтрованный по sshd](/images/Part9_2.png)

- Скрин с процессом syslog, найденным, используя поиск

![Поиск syslog](/images/Part9_3.png)

 - Скрин с добавленным выводом hostname, clock и uptime

![Добавленный вывод hostname, uptime и clock](/images/Part9_4.png)


## Part 10. Использование утилиты fdisk

- Название: /dev/sda
- Размер: 25 GiB
- Кол-во секторов: 52428800
- Размер swap: 2,3G

## Part 11. Использование утилиты df

Запустил команду df

- размер раздела - 12G
- размер занятого пространства - 5,5G
- размер свободного пространства - 5,3G
- процент использования - 52%
- Единицу измерения в выводе - Gb

Запустил команду df -Th.

- размер раздела - 12G
- размер занятого пространства - 5,5G
- размер свободного пространства - 5,3G
- процент использования - 52%
- Тип файловой системы для раздела - ext4

## Part 12. Использование утилиты du

Запустил команду du.

- Рзмер /home - (80) 80K. /var - (759168) 742M. /var/log - (44320) 44M.

![home в байтах](/images/Part12_6.png)
![home в чел](/images/Part12_5.png)
![var в байтах](/images/Part12_3.png)
![var в чел](/images/Part12_4.png)
![var/log в байтах](/images/Part12_2.png)
![var/log в чел](/images/Part12_1.png)

- Размер всего содержимого в /var/log

![var/log содержимое](/images/Part12_7.png)

## Part 13. Установка и использование утилиты ncdu

- Размер /home

![Размер home](/images/Part13_1.png)

- Размер /var

![Размер var](/images/Part13_2.png)

- Размер /var/log

![Размер var/log](/images/Part13_3.png)

## Part 14. Работа с системными журналами

- Время последней успешной авторизации: Tue Jan 17 01:14. Имя пользователя: kentbaha. Метод входа в систему: tty1.

- Перезапустил службу SSHd.

![Лог перезапуска sshd](/images/Part14_2.png)

## Part 15. Использование планировщика заданий CRON

- Устновка скрипта в cron:

![Устновка run.sh](/images/Part15_1.png)

- Проверка работы:

![Проверка](/images/Part15_2.png)

- Выполнение в журналах:

![Журнал](/images/Part15_3.png)

- Удаление задний: crontab -r. Вывод crontab -l:

![ОТчет об удалении](/images/Part15_4.png)
